import React from 'react';

const PlacesLocatorCard = ({ place }) => {
    const { title, street, city, postalcode, id, distanceToSearchLocation } = place;
    const distance = distanceToSearchLocation ? (distanceToSearchLocation / 1000).toFixed(1) : undefined;

    return (
        <div className="places-locator-card" id={ id }>
            <div className="places-locator-card__main">
                <div className="places-locator-card__title">{ title }</div>
                <div className="places-locator-card__address">{ street }, { postalcode } { city } { distance ? `(${distance} km)` : null }</div>
            </div>
        </div>
    );
};

export default PlacesLocatorCard;
