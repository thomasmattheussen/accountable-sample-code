import React from 'react';

const PlacesLocatorListItem = React.forwardRef(({ id, onClick, children, ...other }, ref) => {
    return (
        <li ref={ ref } onClick={ () => onClick({ id }) } { ...other }>
            { children }
        </li>
    );
});

export default PlacesLocatorListItem;
