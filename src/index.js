import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import GoogleMapLoader from './maps/google-map-loader';

import Locator from './locator';

const App = () => {
    const [places, setPlaces] = useState([]);

    useEffect(() => {
        fetch('/agencies.json')
            .then((body) => body.json())
            .then(setPlaces);
    }, []);

    return (
        <Locator
            agencyData={ places }
            apiKey="AIzaSyC6sfQfb2WPIGPFPhsRXMlEjHnw80ZOJ5U"
            loadingElement={ <GoogleMapLoader /> }
        />
    );
}

ReactDOM.render(<App />, document.getElementById('root'));
