/* global google */

import React, { useEffect, useRef } from 'react';

const AutoComplete = ({ options, onPlaceChange, ...other }) => {
    const input = useRef(null);

    useEffect(() => {
        const autoComplete = new google.maps.places.Autocomplete(input.current, options);
        autoComplete.addListener('place_changed', () => {
            onPlaceChange(autoComplete.getPlace());
        });

        return () => {
            google.maps.event.clearInstanceListeners(autoComplete);
        }
    }, [onPlaceChange, options]);

    return (
        <input type="text" ref={ input } className="form__control" { ...other } />
    );
}

export default AutoComplete;




