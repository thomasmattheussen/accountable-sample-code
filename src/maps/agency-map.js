import React from 'react';

import GoogleMap from './google-map';
import Marker from './marker';
import snazzyMapStyle from './map-styles.json';

export default ({ agencies, children, options, ...other }) => {
    return (
        <GoogleMap
            mapContainerClassName="google-map"
            options={ Object.assign({}, { styles: snazzyMapStyle }, options) }
            { ...other }
        >
            {
                agencies.map(({ lat, lng, title, ...otherProps }, ndx) => {
                    return (
                        <Marker position={{ lat, lng }} label={ title } { ...otherProps } key={ ndx } />
                    );
                })
            }
            { children }
        </GoogleMap>
    );
};
