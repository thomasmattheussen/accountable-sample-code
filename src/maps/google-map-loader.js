import React from 'react';

const Spinner = ({ color = "#000000", gap = 2, thickness = 4, size = 64, ...other }) => (
    <div className="locator">
        <div className="google-map__loader">
            <svg
                height={ size }
                width={ size }
                { ...other }
                style={{ animationDuration: '900ms' }}
                role="img"
                viewBox="0 0 32 32"
                className="spinner"
            >
                <circle
                    role="presentation"
                    cx={ 16 }
                    cy={ 16 }
                    r={ 14 - thickness / 2 }
                    stroke={ color }
                    fill="none"
                    strokeWidth={ thickness }
                    strokeDasharray={ Math.PI * 2 * (11 - gap) }
                    strokeLinecap="round"
                />
            </svg>
        </div>
    </div>
);

export default Spinner;
