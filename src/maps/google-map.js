import React, { useCallback } from 'react';

import { GoogleMap } from '@react-google-maps/api';

export default ({ children, onMapLoaded, zoom = 5, center = { lat: 50.5039, lng: 4.4699 }, ...other } = {}) => (
    <GoogleMap
        onLoad={ useCallback(onMapLoaded) }
        zoom={ zoom }
        defaultCenter={ center }
        { ... other }
    >
        { children }
    </GoogleMap>
);
