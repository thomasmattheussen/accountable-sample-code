import React from 'react';

import { Marker } from '@react-google-maps/api';

export default ({ label = '', ...other } = {}) => (
    <Marker
        icon={{
            labelOrigin: {
                x: 15,
                y: 55,
            },
            url: '/images/marker.svg',
            scaledSize: {
                width: 30,
                height: 40,
            },
        }}
        label={{
            color: '#6d4ad3',
            text: label,
            fontWeight: '800',
            fontSize: '14px',
        }}
        { ...other }
    />
);
