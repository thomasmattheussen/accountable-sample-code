/* global google */

export const sortLocationsByDistanceToPoint = (locations, point, desc = false) => {
    return locations
        .map((location) => {
            location.distanceToSearchLocation = google.maps.geometry.spherical.computeDistanceBetween(point, new google.maps.LatLng(location.lat, location.lng));

            return location;
        })
        .sort((a, b) => {
            if (desc) {
                return a.distanceToSearchLocation + b.distanceToSearchLocation;
            }

            return a.distanceToSearchLocation - b.distanceToSearchLocation;
        });
};

export const distanceBetweenPoints = (a, b) => {
    return google.maps.geometry.spherical.computeDistanceBetween(a, b);
};
