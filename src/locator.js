import React, { Component } from 'react';

import { LoadScript } from '@react-google-maps/api';
import debounce from 'lodash/debounce';
import omit from 'lodash/omit';
import cx from 'classnames';

import GoogleMapLoader from './maps/google-map-loader';
import PlacesAutoComplete from './maps/places-autocomplete';
import AgencyMap from './maps/agency-map';
import { sortLocationsByDistanceToPoint } from './maps/utils';

import PlacesLocatorCard from './places-locator/places-locator-card';
import PlacesLocatorListItem from './places-locator/places-locator-list-item';

const loadExtraLibraries = ['geometry', 'places'];

const MIN_ZOOM = 12;
const DEFAULT_MAP_CENTER = { lat: 50.846609, lng: 4.355476 };

const mobileExperience = window.matchMedia('(max-width: 991px)');

const Locator = class extends Component {
    constructor(props) {
        super(props);
        this.handlePlacesChange = this.handlePlacesChange.bind(this);
        this.handleZoomChange = this.handleZoomChange.bind(this);
        this.handleBoundsChange = debounce(this.handleBoundsChange.bind(this), 200);
        this.handleMapLoad = this.handleMapLoad.bind(this);
        this.handleMarkerClick = this.handleMarkerClick.bind(this);
        this.handleListItemClick = this.handleListItemClick.bind(this);

        this.geolocationTries = 0;

        mobileExperience.addListener(() => {
            if (mobileExperience.matches) {
                this.map = null;
            }

            this.forceUpdate();
        });

        this.state = {
            zoom: MIN_ZOOM,
            activeAgencyId: -1,
            place: null,
        };
    }

    centerMapToLocation() {
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition(({ coords }) => {
                if (this.map) {
                    this.map.panTo({ lat: coords.latitude, lng: coords.longitude });
                    this.setState({
                        zoom: 13,
                    });
                } else if (this.geolocationTries < 10) {
                    const timeout = this.geolocationTries < 3 ? 100 : this.geolocationTries * 50;

                    setTimeout(() => {
                        this.centerMapToLocation();
                    }, timeout);

                    this.geolocationTries++;
                }
            });
        }
    }

    componentDidMount() {
        this.centerMapToLocation();
    }

    handlePlacesChange(place) {
        if (!place.geometry) {
            console.error('Place not found...');
            return;
        }

        const newState = {};

        if (this.map) {
            this.map.panTo(place.geometry.location);

            newState.zoom = MIN_ZOOM;
        }

        newState.place = place;

        this.setState(newState);
    }

    handleZoomChange() {
        if (this.map) {
            const nextZoom = this.map.getZoom();
            if (nextZoom !== this.state.zoom) {
                this.setState({
                    zoom: nextZoom,
                });
            }
        }
    }

    handleBoundsChange() {
        this.forceUpdate();
    }

    handleMarkerClick(data) {
        const { id } = data;

        this[`list-${id}`].scrollIntoView({ behavior: 'smooth', block: 'nearest', inline: 'start' });

        this.setState({
            activeAgencyId: id,
        });
    }

    handleMapLoad(map) {
        if (!map || this.map) return;

        this.map = map;
    }

    handleListItemClick({ id }) {
        this.setState({
            activeAgencyId: id,
        });
    }

    render() {
        const { agencyData } = this.props;

        let visibleAgencies = [];

        if (agencyData) {
            visibleAgencies = this.state.place ? sortLocationsByDistanceToPoint(agencyData, this.state.place.geometry.location) : agencyData;

            if (!mobileExperience.matches && this.map && this.map.getBounds()) {
                visibleAgencies = sortLocationsByDistanceToPoint(
                    agencyData.filter(
                        ({ lat, lng }) => this.map.getBounds().contains({ lat, lng })
                    ),
                    this.state.place ? ( this.state.place.geometry.location ) : ( this.map.getCenter() )
                );
            }
        }

        return (
            <LoadScript
                googleMapsApiKey={ this.props.apiKey }
                libraries={ loadExtraLibraries }
                loadingElement= { this.props.loadingElement }
            >
                {
                    agencyData ? (
                        <div className="places-locator">
                            <div className="places-locator__controls">
                                <div className="places-locator__control form">
                                    <div className="places-locator__control__input">
                                        <label htmlFor="agency-address-autocomplete" className="form__label">Search for an address</label>
                                        <PlacesAutoComplete
                                            id="agency-address-autocomplete"
                                            onPlaceChange={ this.handlePlacesChange }
                                            options={{ componentRestrictions: { country: 'be' }}}
                                            placeholder="City, postal code, street, ..."
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="places-locator__main">
                                <div className="places-locator__vacancies">
                                    {
                                        this.state.zoom < MIN_ZOOM ? (
                                            <ul className="places-locator__list">
                                                <li className="places-locator__list-item">
                                                    <span className="h3">Zoom in to see results</span>
                                                </li>
                                            </ul>
                                        ) : (
                                            <ul className="places-locator__list">
                                                {
                                                    visibleAgencies.map((agency) => ({ ...agency, url: agency.uri })).map((agency) => (
                                                        <PlacesLocatorListItem
                                                            id={ agency.id }
                                                            key={ agency.id }
                                                            ref={ (c) => { this[`list-${agency.id}`] = c; } }
                                                            className={ cx('places-locator__list-item', { 'is-active': this.state.activeAgencyId === agency.id }) }
                                                            onClick={ this.handleListItemClick }
                                                        >
                                                            <PlacesLocatorCard place={ this.state.place ? agency : omit(agency, ['distanceToSearchLocation', 'id']) } lblCta="See more details" />
                                                        </PlacesLocatorListItem>
                                                    ))
                                                }
                                            </ul>
                                        )
                                    }
                                </div>
                                <div className="places-locator__map" style={ mobileExperience.matches ? 'display: none;' : null }>
                                    <AgencyMap
                                        zoom={ this.state.zoom }
                                        defaultCenter={ DEFAULT_MAP_CENTER }
                                        agencies={
                                            this.state.zoom < MIN_ZOOM ? (
                                                []
                                            ) : (
                                                visibleAgencies.map((agency) => Object.assign({}, agency, {
                                                    onClick: () => this.handleMarkerClick(agency),
                                                    title: (this.state.activeAgencyId === agency.id) ? agency.title : ' '
                                                }))
                                            )
                                        }
                                        onZoomChanged={ this.handleZoomChange }
                                        onBoundsChanged={ this.handleBoundsChange }
                                        onMapLoaded={ this.handleMapLoad }
                                    />
                                </div>
                            </div>
                        </div>
                    ) : (
                        <GoogleMapLoader />
                    )
                }
            </LoadScript>
        );
    }
};

export default Locator;
